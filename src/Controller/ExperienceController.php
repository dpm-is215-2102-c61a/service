<?php

namespace App\Controller;

use App\Classess\ResponseController;
use App\Entity\Experience;
use App\Entity\Favorite;
use App\Entity\Location;
use App\Entity\Review;
use App\Repository\CategoryRepository;
use App\Repository\ExperienceRepository;
use App\Repository\FavoriteRepository;
use App\Repository\FeatureRepository;
use App\Repository\LocationRepository;
use App\Repository\ReviewRepository;
use App\Repository\UserRepository;
use App\Traits\UtilTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/experience")
 */
class ExperienceController extends AbstractController
{
    use UtilTrait;

    /**
     * @var ResponseController
     */
    protected $response;

    /**
     * @var ExperienceRepository
     */
    private $repository;

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var FeatureRepository
     */
    private $featureRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    /**
     * @var FavoriteRepository
     */
    private $favoriteRepository;

    /**
     * @var LocationRepository
     */
    private $locationRepository;

    /**
     * @param ExperienceRepository $repository
     * @param CategoryRepository $categoryRepository
     * @param FeatureRepository $featureRepository
     * @param UserRepository $userRepository
     * @param ReviewRepository $reviewRepository
     * @param FavoriteRepository $favoriteRepository
     * @param LocationRepository $locationRepository
     */
    public function __construct(ExperienceRepository $repository, CategoryRepository $categoryRepository, FeatureRepository $featureRepository, UserRepository $userRepository, ReviewRepository $reviewRepository, FavoriteRepository $favoriteRepository, LocationRepository $locationRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
        $this->featureRepository = $featureRepository;
        $this->userRepository = $userRepository;
        $this->reviewRepository = $reviewRepository;
        $this->favoriteRepository = $favoriteRepository;
        $this->locationRepository = $locationRepository;
        $this->response = new ResponseController();
    }

    /**
     * @Route("", name="experience_list", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $experiences = $this->repository->all();
        $experiences = $this->experiencesToArray($experiences);

        return $this->json(
            $experiences
        );
    }

    /**
     * @Route("/popular", name="experience_popular_list", methods={"GET"})
     * @return Response
     */
    public function popular(): Response
    {
        $experiences = $this->repository->popular();
        $experiences = $this->experiencesToArray($experiences);

        return $this->json(
            $experiences
        );
    }

    /**
     * @Route("/promotion", name="experience_promotion_list", methods={"GET"})
     * @return Response
     */
    public function promotion(): Response
    {
        $experiences = $this->repository->promotion();
        $experiences = $this->experiencesToArray($experiences);

        return $this->json(
            $experiences
        );
    }

    /**
     * @Route("/{id}", name="experience_show", methods={"GET"})
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        $experience = $this->repository->byId($id);

        return $this->json(
            is_null($experience) ? [] : $experience->toArray()
        );
    }

    /**
     * @Route("", name="experience_save", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $category = $this->categoryRepository->byId($data['category']);
        if (is_null($category)) {
            return $this->json(
                []
            );
        }

        $name = $data['name'];
        $title = $data['title'];
        $description = $data['description'];
        $place = $data['place'];
        $price = number_format($data['price'], 2);
        $image = $data['image'];
        $detail = $data['detail'];
        $expiration = \DateTime::createFromFormat('Y-m-d', $data['expiration']);

        $featuresIds = $this->featuresIdsFromArray($data['features']);
        $features = $this->featureRepository->byIds($featuresIds);

        $sold = $data['sold'];

        $popular = $data['popular'];
        $promotion = $data['promotion'];

        $experience = new Experience();
        $experience->setCategory($category);
        $experience->setCategory($category);
        $experience->setName($name);
        $experience->setTitle($title);
        $experience->setDescription($description);
        $experience->setPlace($place);
        $experience->setPrice($price);
        $experience->setImage($image);
        $experience->setDetail($detail);
        $experience->setExpiration($expiration);

        foreach ($features as $feature) {
            $experience->addFeature($feature);
        }

        $experience->setSold($sold);
        $experience->setPopular($popular);
        $experience->setPromotion($promotion);

        $experience = $this->repository->save($experience);

        if (!empty($data['locations'])) {
            foreach ($data['locations'] as $item) {
                if (!isset($item['name']) || !isset($item['latitude']) || !isset($item['longitude'])) {
                    continue;
                }
                $name = $item['name'];
                $latitude = $item['latitude'];
                $longitude = $item['longitude'];

                $location = new Location();
                $location->setName($name);
                $location->setLatitude($latitude);
                $location->setLongitude($longitude);
                $location->setName($name);
                $location->setExperience($experience);
                $location = $this->locationRepository->save($location);
                $experience->addLocation($location);
            }
        }

        return $this->json(
            is_null($experience) ? [] : $experience->toArray()
        );
    }

    /**
     * @Route("/{id}", name="experience_update", methods={"PUT"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $experience = $this->repository->byId($id);
        if (is_null($experience)) {
            return $this->json(
                []
            );
        }

        if (!empty($data['category'])) {
            $category = $this->categoryRepository->byId($data['category']);
            if (!is_null($category)) {
                $experience->setCategory($category);
            }
        }

        if (!empty($data['name'])) {
            $experience->setName($data['name']);
        }

        if (!empty($data['title'])) {
            $experience->setTitle($data['title']);
        }

        if (!empty($data['description'])) {
            $experience->setDescription($data['description']);
        }

        if (!empty($data['place'])) {
            $experience->setPlace($data['place']);
        }

        if (!empty($data['price'])) {
            $experience->setPrice(number_format($data['price'], 2));
        }

        if (!empty($data['image'])) {
            $experience->setImage($data['image']);
        }

        if (!empty($data['detail'])) {
            $experience->setDetail($data['detail']);
        }

        if (!empty($data['expiration'])) {
            $experience->setExpiration($data['expiration']);
        }

        if (!empty($data['features'])) {
            $featuresIds = $this->featuresIdsFromArray($data['features']);
            $features = $this->featureRepository->byIds($featuresIds);
            $currentFeatures = $experience->getFeatures();
            foreach ($currentFeatures as $currentFeature) {
                $experience->removeFeature($currentFeature);
            }
            foreach ($features as $feature) {
                $experience->addFeature($feature);
            }
        }

        if (!empty($data['locations'])) {
            $newLocationsIds = [];
            foreach ($data['locations'] as $item) {
                if (!isset($item['name']) || !isset($item['latitude']) || !isset($item['longitude'])) {
                    continue;
                }
                $name = $item['name'];
                $latitude = $item['latitude'];
                $longitude = $item['longitude'];
                $location = $this->locationRepository->byLatitudeLongitudeAndExperience($latitude, $longitude, $experience);
                if (is_null($location)) {
                    $location = new Location();
                    $location->setName($name);
                    $location->setLatitude($latitude);
                    $location->setLongitude($longitude);
                    $location->setName($name);
                    $location->setExperience($experience);
                    $location = $this->locationRepository->save($location);
                } else {
                    $location->setName($name);
                    $location = $this->locationRepository->update($location);
                }
                $newLocationsIds[] = $location->getId();
            }

            $currentLocations = $experience->getLocations();
            foreach ($currentLocations as $currentLocation) {
                if (!in_array($currentLocation->getId(), $newLocationsIds)) {
                    $this->locationRepository->delete($currentLocation);
                }
            }
        }

        if (!empty($data['sold'])) {
            $experience->setSold($data['sold']);
        }

        if (array_key_exists('popular', $data)) {
            $experience->setPopular($data['popular']);
        }

        if (array_key_exists('promotion', $data)) {
            $experience->setPromotion($data['promotion']);
        }

        $experience = $this->repository->update($experience);

        return $this->json(
            is_null($experience) ? [] : $experience->toArray()
        );
    }

    /**
     * @Route("/{id}", name="experience_delete", methods={"DELETE"})
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $experience = $this->repository->byId($id);
        if (is_null($experience)) {
            return $this->json(
                false
            );
        }

        $this->repository->delete($experience);

        return $this->json(
            true
        );
    }

    /**
     * @Route("/{id}/reviews", name="experience_reviews", methods={"GET"})
     * @param int $id
     * @return Response
     */
    public function reviews(int $id): Response
    {
        $experience = $this->repository->byId($id);
        if (is_null($experience)) {
            return $this->json(
                $this->response->get()
            );
        }

        $reviews = $this->reviewsToArray($this->reviewRepository->byExperience($experience), true, true);

        return $this->json(
            $reviews
        );
    }

    /**
     * @Route("/{id}/reviews", name="experience_add_review", methods={"POST", "PUT"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function addReview(Request $request, int $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $experience = $this->repository->byId($id);
        if (is_null($experience)) {
            return $this->json(
                false
            );
        }

        if (empty($data['user'])) {
            return $this->json(
                false
            );
        }

        $user = $this->userRepository->byId($data['user']);
        if (is_null($user)) {
            return $this->json(
                false
            );
        }

        $review = $this->reviewRepository->byUserAndExperience($user, $experience);
        if (is_null($review) and !isset($data['score']) and !isset($data['comment'])) {
            return $this->json(
                false
            );
        }

        if (is_null($review)) {
            $review = new Review();
        }

        if (isset($data['score'])) {
            $review->setScore(number_format($data['score'], 1));
        }

        if (isset($data['comment'])) {
            $review->setComment($data['comment']);
        }

        $review->setUser($user);
        $review->setExperience($experience);
        $this->reviewRepository->save($review);

        $rating = $this->reviewRepository->avgByExperience($experience);
        $experience->setRating($rating);

        $this->repository->save($experience);

        return $this->json(
            true
        );
    }

    /**
     * @Route("/{id}/favorites", name="experience_favorites", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function favorites(string $id): Response
    {
        $user = $this->userRepository->byId($id);
        if (is_null($user)) {
            return $this->json(
                []
            );
        }

        $favorites = $this->repository->favorites($user);
        $favorites = $this->experiencesToArray($favorites);

        return $this->json(
            $favorites
        );
    }

    /**
     * @Route("/{id}/favorites/{userid}", name="experience_add_favorite", methods={"POST", "PUT"})
     * @param Request $request
     * @param int $id
     * @param string $userid
     * @return Response
     */
    public function addFavorite(Request $request, int $id, string $userid): Response
    {
        $data = json_decode($request->getContent(), true);

        $experience = $this->repository->byId($id);
        if (is_null($experience)) {
            return $this->json(
                false
            );
        }

        $user = $this->userRepository->byId($userid);
        if (is_null($user)) {
            return $this->json(
                false
            );
        }

        $favorite = $this->favoriteRepository->byUserAndExperience($user, $experience);
        if (!is_null($favorite)) {
            return $this->json(
                true
            );
        }

        $favorite = new Favorite();
        $favorite->setUser($user);
        $favorite->setExperience($experience);
        $this->favoriteRepository->save($favorite);

        return $this->json(
            true
        );
    }

    /**
     * @Route("/{id}/favorites/{userid}", name="experience_remove_favorite", methods={"DELETE"})
     * @param Request $request
     * @param int $id
     * @param string $userid
     * @return Response
     */
    public function removeFavorite(Request $request, int $id, string $userid): Response
    {
        $experience = $this->repository->byId($id);
        if (is_null($experience)) {
            return $this->json(
                false
            );
        }

        $user = $this->userRepository->byId($userid);
        if (is_null($user)) {
            return $this->json(
                false
            );
        }

        $favorite = $this->favoriteRepository->byUserAndExperience($user, $experience);
        if (is_null($favorite)) {
            return $this->json(
                false
            );
        }

        $this->favoriteRepository->delete($favorite);

        return $this->json(
            true
        );
    }
}

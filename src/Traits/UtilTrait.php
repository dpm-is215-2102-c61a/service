<?php

namespace App\Traits;

use App\Entity\Category;
use App\Entity\Experience;
use App\Entity\Feature;
use App\Entity\Location;
use App\Entity\Review;
use App\Entity\User;

trait UtilTrait
{
    /**
     * @param Category[] $categories
     * @return array
     */
    public function categoriesToArray(array $categories): array
    {
        $array = [];
        foreach ($categories as $category) {
            $array[] = $category->toArray();
        }
        return $array;
    }

    /**
     * @param Feature[] $features
     * @return array
     */
    public function featuresToArray(array $features): array
    {
        $array = [];
        foreach ($features as $feature) {
            $array[] = $feature->toArray();
        }
        return $array;
    }

    /**
     * @param Location[] $locations
     * @return array
     */
    public function locationsToArray(array $locations): array
    {
        $array = [];
        foreach ($locations as $location) {
            $array[] = $location->toArray();
        }
        return $array;
    }

    /**
     * @param array $features
     * @return int[]
     */
    public function featuresIdsFromArray(array $features): array
    {
        $ids = [];
        foreach ($features as $feature) {
            if (isset($feature['id'])) {
                $ids[] = (int)$feature['id'];
            }
        }
        return $ids;
    }

    /**
     * @param Experience[] $experiences
     * @return array
     */
    public function experiencesToArray(array $experiences): array
    {
        $array = [];
        foreach ($experiences as $experience) {
            $array[] = $experience->toArray();
        }
        return $array;
    }

    /**
     * @param Review[] $reviews
     * @param bool $includeUser
     * @param bool $includeExperience
     * @return array
     */
    public function reviewsToArray(array $reviews, bool $includeUser, bool $includeExperience): array
    {
        $array = [];
        foreach ($reviews as $review) {
            $array[] = $review->toArray($includeUser, $includeExperience);
        }
        return $array;
    }

    /**
     * @param User[] $users
     * @return array
     */
    public function usersToArray(array $users): array
    {
        $array = [];
        foreach ($users as $user) {
            $array[] = $user->toArray();
        }
        return $array;
    }
}

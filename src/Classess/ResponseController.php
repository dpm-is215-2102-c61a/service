<?php

namespace App\Classess;

class ResponseController
{
    /**
     * @var bool
     */
    protected $status;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var string
     */
    protected $message;

    public function __construct(bool $status = false, $data = [], string $message = "error")
    {
        $this->status = $status;
        $this->data = $data;
        $this->message = $message;
    }

    public function get()
    {
        return [
            'status' => $this->isStatus(),
            'data' => $this->getData(),
            'message' => $this->getMessage(),
        ];
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return ResponseController
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return ResponseController
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return ResponseController
     */
    public function setMessage(string $message): self
    {
        $this->message = $message;
        return $this;
    }
}

<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, User::class);
        $this->em = $manager;
    }

    /**
     * @param User $user
     * @return User
     */
    public function save(User $user): User
    {
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    public function update(User $user): User
    {
        $this->em->persist($user);
        $this->em->flush();
        return $user;
    }

    /**
     * @param User $user
     */
    public function delete(User $user): void
    {
        $this->em->remove($user);
        $this->em->flush();
    }

    /**
     * @param string $id
     * @return User|null
     */
    public function byId(string $id): ?User
    {
        $q = $this->createQueryBuilder('u')
            ->where('u.uuid = :id')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $id
     * @return User|null
     */
    public function userById(string $id): ?User
    {
        $q = $this->createQueryBuilder('u')
            ->where('u.uuid = :id')
            ->andWhere('u.is_admin = 0')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param string $id
     * @return User|null
     */
    public function administratorById(string $id): ?User
    {
        $q = $this->createQueryBuilder('u')
            ->where('u.uuid = :id')
            ->andWhere('u.is_admin = 1')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return User[]
     */
    public function users(): array
    {
        $q = $this->createQueryBuilder('u')
            ->where('u.is_admin = 0')
            ->orderBy('u.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @return User[]
     */
    public function administrators(): array
    {
        $q = $this->createQueryBuilder('u')
            ->where('u.is_admin = 1')
            ->orderBy('u.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @return User[]
     */
    public function all(): array
    {
        $q = $this->createQueryBuilder('u')
            ->orderBy('u.name', 'ASC');
        return $q->getQuery()->getResult();
    }
}

<?php

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Category|null find($id, $lockMode = null, $lockVersion = null)
 * @method Category|null findOneBy(array $criteria, array $orderBy = null)
 * @method Category[]    findAll()
 * @method Category[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Category::class);
        $this->em = $manager;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category
    {
        $this->em->persist($category);
        $this->em->flush();
        return $category;
    }

    /**
     * @param Category $category
     * @return Category
     */
    public function update(Category $category): Category
    {
        $this->em->persist($category);
        $this->em->flush();
        return $category;
    }

    /**
     * @param Category $category
     */
    public function delete(Category $category): void
    {
        $this->em->remove($category);
        $this->em->flush();
    }

    /**
     * @param int $id
     * @return Category|null
     */
    public function byId(int $id): ?Category
    {
        $q = $this->createQueryBuilder('c')
            ->where('c.id = :id')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return Category[]
     */
    public function all(): array
    {
        $q = $this->createQueryBuilder('c')
            ->orderBy('c.name', 'ASC');
        return $q->getQuery()->getResult();
    }
}

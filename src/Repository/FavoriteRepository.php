<?php

namespace App\Repository;

use App\Entity\Experience;
use App\Entity\Favorite;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Favorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favorite[]    findAll()
 * @method Favorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriteRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Favorite::class);
        $this->em = $manager;
    }

    /**
     * @param Favorite $favorite
     * @return Favorite
     */
    public function save(Favorite $favorite): Favorite
    {
        $this->em->persist($favorite);
        $this->em->flush();
        return $favorite;
    }

    /**
     * @param Favorite $favorite
     * @return Favorite
     */
    public function update(Favorite $favorite): Favorite
    {
        $this->em->persist($favorite);
        $this->em->flush();
        return $favorite;
    }

    /**
     * @param Favorite $favorite
     */
    public function delete(Favorite $favorite) {
        $this->em->remove($favorite);
        $this->em->flush();
    }

    /**
     * @param User $user
     * @param Experience $experience
     * @return Favorite|null
     */
    public function byUserAndExperience(User $user, Experience $experience): ?Favorite
    {
        $q = $this->createQueryBuilder('f')
            ->join('f.user', 'u')
            ->join('f.experience', 'e')
            ->where('u.id = :user')
            ->andWhere('e.id = :experience')
            ->setParameters(['user' => $user->getId(), 'experience' => $experience->getId()]);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}

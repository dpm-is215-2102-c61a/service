-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: gift
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Diversión','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/category%2F6beeece2-d701-4097-ad13-745ca703a765.png?alt=media&token=7946d1d4-d0f6-4e39-9def-76728ea797ab'),(2,'Aventura','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/category%2FF13gxBEXJYS2L0DFUnghX6DKT7s1.jpg?alt=media&token=4f080467-6a39-4760-82ee-7792fc09ae36'),(3,'Gastronomia','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/category%2Fd87b0f5c-5c30-4b5e-a6c4-d5f612fd4d46.png?alt=media&token=f8fd3fe3-ff60-42e9-8be8-780b90fc29ea'),(4,'Escapadas','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/category%2F36fed769-c5f9-404c-bb14-0b0511bef449.png?alt=media&token=378bc791-3cd2-4510-a79f-a6f1b0925cb0'),(5,'Bienestar','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/category%2F68209598-23db-4110-985e-4f2979f7eb9e.png?alt=media&token=da6e6125-151b-4d0f-a493-2ff056e617fa');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctrine_migration_versions`
--

LOCK TABLES `doctrine_migration_versions` WRITE;
/*!40000 ALTER TABLE `doctrine_migration_versions` DISABLE KEYS */;
INSERT INTO `doctrine_migration_versions` VALUES ('DoctrineMigrations\\Version20210915221822','2021-09-16 09:30:26',33),('DoctrineMigrations\\Version20210916011212','2021-09-16 09:30:26',211),('DoctrineMigrations\\Version20210916031008','2021-09-16 09:30:26',159),('DoctrineMigrations\\Version20210919012412','2021-09-19 02:27:34',55),('DoctrineMigrations\\Version20210925074604','2021-09-25 09:31:31',169),('DoctrineMigrations\\Version20210925114157','2021-09-25 11:44:13',85),('DoctrineMigrations\\Version20210926113305','2021-09-26 12:07:59',132);
/*!40000 ALTER TABLE `doctrine_migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experience`
--

DROP TABLE IF EXISTS `experience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `experience` (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `place` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` date NOT NULL,
  `rating` double NOT NULL DEFAULT '0',
  `popular` tinyint(1) NOT NULL DEFAULT '0',
  `promotion` tinyint(1) NOT NULL DEFAULT '0',
  `sold` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_590C10312469DE2` (`category_id`),
  CONSTRAINT `FK_590C10312469DE2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experience`
--

LOCK TABLES `experience` WRITE;
/*!40000 ALTER TABLE `experience` DISABLE KEYS */;
INSERT INTO `experience` VALUES (1,5,'Spa romantico en pareja','Experience 2D / 1N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','San Isidro - Lima','250.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Fspa.png?alt=media&token=eed930a7-ddeb-48b8-a185-0d5282ac66546','Lorem ipsum dolor sit amet.','2021-12-12',4.1,1,1,120),(2,4,'VIERNES 01 OCT.- RENUNCIO','Experience 2D / 1N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','ONLINE','70.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Frenuncio.png?alt=media&token=396d18ac-97ba-4b12-9f96-8460889bbc9c','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,129),(3,4,'VIERNES 05 NOV.- MUEVETE SIN ROCHE','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','ONLINE','220.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2FMUEVETE.png?alt=media&token=5891f0dd-f1cd-4486-9f83-3e70a504365b','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,64),(4,5,'SmartFit Black 3 meses %50 desc.','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','San Borja - Lima','150.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Fsmart.png?alt=media&token=608f2453-4e8b-442f-8038-8ccd669b66f0','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,312),(5,1,'SABADO 30 OCT.-LA FIESTA DEL PERRO','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','Toro Bar - Barranco','77.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Ffiestadelperro.png?alt=media&token=d598b772-4d7e-47e7-94a9-12a2d567c004','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,546),(6,3,'50% DESCUENTO EN PARRILLAS','Experience 06','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','KILO - San Isidro','70.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2FPROMO7.jpg?alt=media&token=20d2167f-ff7c-4913-968f-3bd57f63bf62','Lorem ipsum dolor sit amet.','2021-12-25',0,0,1,164),(7,1,'SABADO 25 SET.-NOCHE DE ESTRELLAS','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','Del Carajo - Barranco','120.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Fnochedeestrellas.png?alt=media&token=5762e8fb-9349-4084-8274-241810287bc8','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,312),(8,2,'Conoce Arequipa en pareja 4D/3N','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','Arequipa - Perú','1,200.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Farequipa.png?alt=media&token=f943e68b-acf8-452e-ae4d-2fd7cc343707','Lorem ipsum dolor sit amet.\nLorem ipsum dolor sit amet.\nLorem ipsum dolor sit amet.\nLorem ipsum dolor sit amet.','2021-12-25',0,0,1,209),(9,3,'PROMO COMBO SODEXO PERSONAL','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','China Wok - ONLINE','12.90','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Fpromo%20chifa.png?alt=media&token=ea6e88fd-8796-4976-a64f-3b66491d0d32','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,484),(10,1,'SABADO 09 OCT.- RIO','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','Lima 39 - Los Olivos','70.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Frio.png?alt=media&token=1dc4ff69-ac76-43de-91af-704f010f3be9','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,129),(11,2,'Full Day dunas Huacachina','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','Ica - Perú','150.00','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Fexperiencia-1.png?alt=media&token=7b308627-3f36-44e2-8618-ca1b37434076','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,987),(12,3,'PROMO PIZZA MEDIANA 50% DSCTO.','Experience 4D / 3N Tour','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut volutpat ex, ut malesuada velit. Proin ex erat, ornare in.','Pizza Hut - ONLINE','10.90','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/experience%2Fpromo%20pizza.jpg?alt=media&token=5c5eca0a-0953-4b5c-a17b-9caf0fcc2250','Lorem ipsum dolor sit amet.','2021-12-25',0,1,1,172);
/*!40000 ALTER TABLE `experience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiences_features`
--

DROP TABLE IF EXISTS `experiences_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `experiences_features` (
  `experience_id` int NOT NULL,
  `feature_id` int NOT NULL,
  PRIMARY KEY (`experience_id`,`feature_id`),
  KEY `IDX_B0CEEC6E46E90E27` (`experience_id`),
  KEY `IDX_B0CEEC6E60E4B879` (`feature_id`),
  CONSTRAINT `FK_B0CEEC6E46E90E27` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_B0CEEC6E60E4B879` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiences_features`
--

LOCK TABLES `experiences_features` WRITE;
/*!40000 ALTER TABLE `experiences_features` DISABLE KEYS */;
INSERT INTO `experiences_features` VALUES (6,3),(6,4),(7,1),(7,2),(8,1),(8,2),(9,1),(9,2),(10,3),(10,4),(11,3),(11,4),(12,3),(12,4);
/*!40000 ALTER TABLE `experiences_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorite`
--

DROP TABLE IF EXISTS `favorite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favorite` (
  `id` int NOT NULL AUTO_INCREMENT,
  `experience_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68C58ED946E90E27` (`experience_id`),
  KEY `IDX_68C58ED9A76ED395` (`user_id`),
  CONSTRAINT `FK_68C58ED946E90E27` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`id`),
  CONSTRAINT `FK_68C58ED9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorite`
--

LOCK TABLES `favorite` WRITE;
/*!40000 ALTER TABLE `favorite` DISABLE KEYS */;
INSERT INTO `favorite` VALUES (15,4,7),(21,1,85);
/*!40000 ALTER TABLE `favorite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feature`
--

DROP TABLE IF EXISTS `feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `feature` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feature`
--

LOCK TABLES `feature` WRITE;
/*!40000 ALTER TABLE `feature` DISABLE KEYS */;
INSERT INTO `feature` VALUES (1,'Rutas','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/feature%2F1208627d-401b-4b6c-bbc4-651f0b0feb5f.png?alt=media&token=0a36e988-6ee8-43d0-a0bb-1f33a86503ea'),(2,'Comida','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/feature%2F5085e054-dc80-478f-bfdd-de2f8c278305.png?alt=media&token=5e88554e-8e23-414f-b798-9b47d29180b8'),(3,'Hotel','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/feature%2F436bb286-73be-49e2-b518-a9aa85caf013.png?alt=media&token=5bb118d0-fa50-442c-baad-27bbe5b797a9'),(4,'Turismo','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/feature%2Fdabe453a-29db-4c10-9eff-a5429c1c2ff5.png?alt=media&token=a1a85652-bf8a-45e4-959a-9686e7899871');
/*!40000 ALTER TABLE `feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location` (
  `id` int NOT NULL AUTO_INCREMENT,
  `experience_id` int DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5E9E89CB46E90E27` (`experience_id`),
  CONSTRAINT `FK_5E9E89CB46E90E27` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,1,'Local 01','-12.0465245','-77.0300855'),(2,1,'Local 04','-12.0460038','-77.0305458'),(3,2,'Local 01','-12.0465245','-77.0300855'),(4,2,'Local 04','-12.0460038','-77.0305458'),(5,6,'San Iisdro','-12.0976278','-77.0384159'),(6,8,'Plaza de armas Arequipa','-16.399102','-71.5377432'),(7,11,'Huacachina, Ica','-14.0876298','-75.7651367'),(8,9,'Rambla San Borja','-12.0977255','-77.014995'),(9,9,'Surco','-12.1240357','-77.0340495'),(10,12,'Rambla San Borja','-12.0977255','-77.014995'),(11,12,'Miraflores','-12.119925','-77.0582845'),(12,10,'Los Olivos','-11.9703403','-77.075936'),(13,7,'Del Carajo','-12.1366671','-77.0190098'),(14,5,'Toro Retro Bar','-12.1464841','-77.0207416'),(15,4,'La Victoria','-12.1091781','-77.0199264'),(16,4,'San Isidro','-12.1150626','-77.0281305');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `review` (
  `id` int NOT NULL AUTO_INCREMENT,
  `experience_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `score` double NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_794381C646E90E27` (`experience_id`),
  KEY `IDX_794381C6A76ED395` (`user_id`),
  CONSTRAINT `FK_794381C646E90E27` FOREIGN KEY (`experience_id`) REFERENCES `experience` (`id`),
  CONSTRAINT `FK_794381C6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,1,1,4.8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec.'),(2,1,2,3.4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi id quam sed.');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649D17F50A6` (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'8Cd5rYsP6leDimTS3VZNwP5abJY2','Raul','S D','rzcodx@gmail.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/user%2F80f8eb87-cac5-40fd-b273-b2c28f579fd5.jpg?alt=media&token=781842c1-7996-48e0-aed8-7c09603ea2fd',1),(2,'9loMh3u2GJS9iduw2LeVtniGhsG2','Mario','Yunis','mario@email.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/user%2F786e6744-58da-4f15-84e5-cd5376a0b805.png?alt=media&token=c07f1cc4-79ab-41fe-b3b0-df8525f77cbe',0),(7,'FAsC37c5jsbiNPR1MspI0KSsee32','Juan','Perez','juan@perez.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/user%2Faeca8380-0a78-4efe-bf34-174f0aa85831.jpg?alt=media&token=91b74447-0c09-4215-929b-b5f45a33aa2f',0),(8,'tD565DY8jsViuLYsBwYMUmZRTPR2','Diana','V F','diana@gmail.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',0),(9,'WlgvXUnnRUgGKqXol8DYdRhsPdv1','Pedro','Flores Díaz','pedro@gmail.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',0),(10,'HbaJP1zA0jbx0Y3qB2ROTGYFyOu2','Lucas','Flores Altos','lucas@gmail.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',0),(82,'eJpJLl826rOwEYEzlZZWmmnvTPb2','Borrar','Borrar Borrar','borrar@aaaa.pe','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',0),(83,'taiW0LhXecchzblWS5ylkRakowt1','Mario','Yunis','mario@admin.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',1),(84,'OkLcYavgvwT9mpJxGrLkOBcs2x73','Estefania','Agurto','estefania.agurto.e@gmail.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',1),(85,'wSFDWvl12GddaF4SKUsCRF7HiS63','Leydy','Peralta','leydy.peralta@gmail.com','https://firebasestorage.googleapis.com/v0/b/is215-2102-c61a-5187d.appspot.com/o/profile_img.png?alt=media&token=e322c288-30b9-46d7-82f5-19251d67b8d6',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-28  1:06:45

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210916011212 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE experience (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, place VARCHAR(255) NOT NULL, price VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, detail LONGTEXT NOT NULL, expiration DATE NOT NULL, INDEX IDX_590C10312469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE experiences_features (experience_id INT NOT NULL, feature_id INT NOT NULL, INDEX IDX_B0CEEC6E46E90E27 (experience_id), INDEX IDX_B0CEEC6E60E4B879 (feature_id), PRIMARY KEY(experience_id, feature_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE feature (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, image VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE experience ADD CONSTRAINT FK_590C10312469DE2 FOREIGN KEY (category_id) REFERENCES category (id)');
        $this->addSql('ALTER TABLE experiences_features ADD CONSTRAINT FK_B0CEEC6E46E90E27 FOREIGN KEY (experience_id) REFERENCES experience (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE experiences_features ADD CONSTRAINT FK_B0CEEC6E60E4B879 FOREIGN KEY (feature_id) REFERENCES feature (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE experiences_features DROP FOREIGN KEY FK_B0CEEC6E46E90E27');
        $this->addSql('ALTER TABLE experiences_features DROP FOREIGN KEY FK_B0CEEC6E60E4B879');
        $this->addSql('DROP TABLE experience');
        $this->addSql('DROP TABLE experiences_features');
        $this->addSql('DROP TABLE feature');
    }
}

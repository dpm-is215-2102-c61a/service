<?php

namespace App\Repository;

use App\Entity\Experience;
use App\Entity\Review;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Review::class);
        $this->em = $manager;
    }

    /**
     * @param Review $review
     * @return Review
     */
    public function save(Review $review): Review
    {
        $this->em->persist($review);
        $this->em->flush();
        return $review;
    }

    /**
     * @param Review $review
     * @return Review
     */
    public function update(Review $review): Review
    {
        $this->em->persist($review);
        $this->em->flush();
        return $review;
    }

    /**
     * @param Review $review
     */
    public function delete(Review $review): void
    {
        $this->em->remove($review);
        $this->em->flush();
    }

    /**
     * @param int $id
     * @return Review|null
     */
    public function byId(int $id): ?Review
    {
        $q = $this->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param Experience $experience
     * @return Review[]
     */
    public function byExperience(Experience $experience): array
    {
        $q = $this->createQueryBuilder('r')
            ->join('r.experience', 'e')
            ->where('e.id = :experience')
            ->setParameter('experience', $experience->getId());
        return $q->getQuery()->getResult();
    }

    public function avgByExperience(Experience $experience)
    {
        $q = $this->createQueryBuilder('r')
            ->join('r.experience', 'e')
            ->select('AVG(r.score) as avg')
            ->where('e.id = :experience')
            ->setParameter('experience', $experience->getId());

        try {
            return $q->getQuery()->getSingleScalarResult();
        } catch (NoResultException | NonUniqueResultException $e) {
            return 0;
        }
    }

    /**
     * @param User $user
     * @param Experience $experience
     * @return Experience|null
     */
    public function byUserAndExperience(User $user, Experience $experience): ?Experience
    {
        $q = $this->createQueryBuilder('r')
            ->join('r.user', 'u')
            ->join('r.experience', 'e')
            ->where('u.id = :user')
            ->andWhere('e.id = :experience')
            ->setParameters(['user' => $user->getId(), 'experience' => $experience->getId()]);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @return Review[]
     */
    public function all(): array
    {
        $q = $this->createQueryBuilder('r')
            ->orderBy('r.name', 'ASC');
        return $q->getQuery()->getResult();
    }
}

<?php

namespace App\Controller;

use App\Classess\ResponseController;
use App\Entity\User;
use App\Repository\AdministratorRepository;
use App\Traits\UtilTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/administrator")
 */
class AdministratorController extends AbstractController
{
    use UtilTrait;

    /**
     * @var ResponseController
     */
    protected $response;

    /**
     * @var AdministratorRepository
     */
    private $repository;

    /**
     * @param AdministratorRepository $repository
     */
    public function __construct(AdministratorRepository $repository)
    {
        $this->repository = $repository;
        $this->response = new ResponseController();
    }

    /**
     * @Route("", name="administrator_list", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $administrators = $this->repository->all();
        $administrators = $this->usersToArray($administrators);

        return $this->json(
            $administrators
        );
    }

    /**
     * @Route("/{id}", name="administrator_show", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function show(string $id): Response
    {
        $administrator = $this->repository->byId($id);

        return $this->json(
            is_null($administrator) ? [] : $administrator->toArray()
        );
    }

    /**
     * @Route("", name="administrator_save", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $uuid = $data['id'];
        $name = $data['name'];
        $lastname = $data['lastname'];
        $email = $data['email'];
        $image = $data['image'];
        $isAdmin = $data['is_admin'];

        $administrator = new User();
        $administrator->setUuid($uuid);
        $administrator->setName($name);
        $administrator->setLastname($lastname);
        $administrator->setEmail($email);
        $administrator->setImage($image);
        $administrator->setIsAdmin($isAdmin);
        $administrator = $this->repository->save($administrator);

        return $this->json(
            is_null($administrator) ? [] : $administrator->toArray()
        );
    }

    /**
     * @Route("/{id}", name="administrator_update", methods={"PUT"})
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function update(Request $request, string $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $administrator = $this->repository->byId($id);
        if (is_null($administrator)) {
            return $this->json(
                []
            );
        }

        if (!empty($data['name'])) {
            $administrator->setName($data['name']);
        }

        if (!empty($data['lastname'])) {
            $administrator->setLastname($data['lastname']);
        }

        if (!empty($data['email'])) {
            $administrator->setEmail($data['email']);
        }

        if (!empty($data['image'])) {
            $administrator->setImage($data['image']);
        }

        if (array_key_exists('is_admin', $data)) {
            $administrator->setIsAdmin($data['is_admin']);
        }

        $administrator = $this->repository->update($administrator);

        return $this->json(
            is_null($administrator) ? [] : $administrator->toArray()
        );
    }

    /**
     * @Route("/{id}", name="administrator_delete", methods={"DELETE"})
     * @param string $id
     * @return Response
     */
    public function delete(string $id): Response
    {
        $administrator = $this->repository->byId($id);
        if (is_null($administrator)) {
            return $this->json(
                false
            );
        }

        $this->repository->delete($administrator);

        return $this->json(
            true
        );
    }
}

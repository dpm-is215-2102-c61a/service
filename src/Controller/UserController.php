<?php

namespace App\Controller;

use App\Classess\ResponseController;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Traits\UtilTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    use UtilTrait;

    /**
     * @var ResponseController
     */
    protected $response;

    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->response = new ResponseController();
    }

    /**
     * @Route("", name="user_list", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $users = $this->repository->all();
        $users = $this->usersToArray($users);

        return $this->json(
            $users
        );
    }

    /**
     * @Route("/users", name="user_list_users", methods={"GET"})
     * @return Response
     */
    public function users(): Response
    {
        $users = $this->repository->users();
        $users = $this->usersToArray($users);

        return $this->json(
            $users
        );
    }

    /**
     * @Route("/administrators", name="user_list_administrators", methods={"GET"})
     * @return Response
     */
    public function administrators(): Response
    {
        $users = $this->repository->administrators();
        $users = $this->usersToArray($users);

        return $this->json(
            $users
        );
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function show(string $id): Response
    {
        $user = $this->repository->byId($id);

        return $this->json(
            is_null($user) ? [] : $user->toArray()
        );
    }

    /**
     * @Route("/{id}/users", name="user_show_user", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function showUser(string $id): Response
    {
        $user = $this->repository->userById($id);

        return $this->json(
            is_null($user) ? [] : $user->toArray()
        );
    }

    /**
     * @Route("/{id}/administrators", name="user_show_administrator", methods={"GET"})
     * @param string $id
     * @return Response
     */
    public function showAdministrator(string $id): Response
    {
        $user = $this->repository->administratorById($id);

        return $this->json(
            is_null($user) ? [] : $user->toArray()
        );
    }

    /**
     * @Route("", name="user_save", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $uuid = $data['id'];
        $name = $data['name'];
        $lastname = $data['lastname'];
        $email = $data['email'];
        $image = $data['image'];
        $isAdmin = $data['is_admin'];

        $user = new User();
        $user->setUuid($uuid);
        $user->setName($name);
        $user->setLastname($lastname);
        $user->setEmail($email);
        $user->setImage($image);
        $user->setIsAdmin($isAdmin);
        $user = $this->repository->save($user);

        return $this->json(
            is_null($user) ? [] : $user->toArray()
        );
    }

    /**
     * @Route("/{id}", name="user_update", methods={"PUT"})
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function update(Request $request, string $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $user = $this->repository->byId($id);
        if (is_null($user)) {
            return $this->json(
                []
            );
        }

        if (!empty($data['name'])) {
            $user->setName($data['name']);
        }

        if (!empty($data['lastname'])) {
            $user->setLastname($data['lastname']);
        }

        if (!empty($data['email'])) {
            $user->setEmail($data['email']);
        }

        if (!empty($data['image'])) {
            $user->setImage($data['image']);
        }

        if (array_key_exists('is_admin', $data)) {
            $user->setIsAdmin($data['is_admin']);
        }

        $user = $this->repository->update($user);

        return $this->json(
            is_null($user) ? [] : $user->toArray()
        );
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     * @param string $id
     * @return Response
     */
    public function delete(string $id): Response
    {
        $user = $this->repository->byId($id);
        if (is_null($user)) {
            return $this->json(
                false
            );
        }

        $this->repository->delete($user);

        return $this->json(
            true
        );
    }
}

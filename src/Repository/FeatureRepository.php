<?php

namespace App\Repository;

use App\Entity\Feature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Feature|null find($id, $lockMode = null, $lockVersion = null)
 * @method Feature|null findOneBy(array $criteria, array $orderBy = null)
 * @method Feature[]    findAll()
 * @method Feature[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeatureRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Feature::class);
        $this->em = $manager;
    }

    /**
     * @param Feature $feature
     * @return Feature
     */
    public function save(Feature $feature): Feature
    {
        $this->em->persist($feature);
        $this->em->flush();
        return $feature;
    }

    /**
     * @param Feature $feature
     * @return Feature
     */
    public function update(Feature $feature): Feature
    {
        $this->em->persist($feature);
        $this->em->flush();
        return $feature;
    }

    /**
     * @param Feature $feature
     */
    public function delete(Feature $feature): void
    {
        $this->em->remove($feature);
        $this->em->flush();
    }

    /**
     * @param int $id
     * @return Feature|null
     */
    public function byId(int $id): ?Feature
    {
        $q = $this->createQueryBuilder('f')
            ->where('f.id = :id')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param int[] $ids
     * @return Feature[]
     */
    public function byIds(array $ids): array
    {
        $q = $this->createQueryBuilder('f')
            ->where('f.id IN (:ids)')
            ->setParameter('ids', $ids);

        return $q->getQuery()->getResult();
    }

    /**
     * @return Feature[]
     */
    public function all(): array
    {
        $q = $this->createQueryBuilder('f')
            ->orderBy('f.name', 'ASC');
        return $q->getQuery()->getResult();
    }
}

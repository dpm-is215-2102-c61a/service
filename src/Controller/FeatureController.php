<?php

namespace App\Controller;

use App\Classess\ResponseController;
use App\Entity\Feature;
use App\Repository\FeatureRepository;
use App\Traits\UtilTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/feature")
 */
class FeatureController extends AbstractController
{
    use UtilTrait;

    /**
     * @var ResponseController
     */
    protected $response;

    private $repository;

    /**
     * @param FeatureRepository $repository
     */
    public function __construct(FeatureRepository $repository)
    {
        $this->repository = $repository;
        $this->response = new ResponseController();
    }

    /**
     * @Route("", name="feature_list", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $features = $this->repository->all();
        $features = $this->featuresToArray($features);

        return $this->json(
            $features
        );
    }

    /**
     * @Route("/{id}", name="feature_show", methods={"GET"})
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        $feature = $this->repository->byId($id);

        return $this->json(
            is_null($feature) ? [] : $feature->toArray()
        );
    }

    /**
     * @Route("", name="feature_save", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $name = $data['name'];
        $image = $data['image'];

        $feature = new Feature();
        $feature->setName($name);
        $feature->setImage($image);
        $feature = $this->repository->save($feature);

        return $this->json(
            is_null($feature) ? [] : $feature->toArray()
        );
    }

    /**
     * @Route("/{id}", name="feature_update", methods={"PUT"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $feature = $this->repository->byId($id);
        if (is_null($feature)) {
            return $this->json(
                []
            );
        }

        if (!empty($data['name'])) {
            $feature->setName($data['name']);
        }

        if (!empty($data['image'])) {
            $feature->setImage($data['image']);
        }

        $feature = $this->repository->update($feature);

        return $this->json(
            is_null($feature) ? [] : $feature->toArray()
        );
    }

    /**
     * @Route("/{id}", name="feature_delete", methods={"DELETE"})
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $feature = $this->repository->byId($id);
        if (is_null($feature)) {
            return $this->json(
                false
            );
        }

        $this->repository->delete($feature);

        return $this->json(
            true
        );
    }
}

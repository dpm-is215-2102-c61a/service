<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\Experience;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Experience|null find($id, $lockMode = null, $lockVersion = null)
 * @method Experience|null findOneBy(array $criteria, array $orderBy = null)
 * @method Experience[]    findAll()
 * @method Experience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExperienceRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Experience::class);
        $this->em = $manager;
    }

    /**
     * @param Experience $experience
     * @return Experience
     */
    public function save(Experience $experience): Experience
    {
        $this->em->persist($experience);
        $this->em->flush();
        return $experience;
    }

    /**
     * @param Experience $experience
     * @return Experience
     */
    public function update(Experience $experience): Experience
    {
        $this->em->persist($experience);
        $this->em->flush();
        return $experience;
    }

    /**
     * @param Experience $experience
     */
    public function delete(Experience $experience): void
    {
        $this->em->remove($experience);
        $this->em->flush();
    }

    /**
     * @param int $id
     * @return Experience|null
     */
    public function byId(int $id): ?Experience
    {
        $q = $this->createQueryBuilder('e')
            ->where('e.id = :id')
            ->setParameter('id', $id);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param Category $category
     * @return Experience[]
     */
    public function byCategory(Category $category): array
    {
        $q = $this->createQueryBuilder('e')
            ->join('e.category', 'c')
            ->where('c.id = :id')
            ->setParameter('id', $category->getId())
            ->orderBy('e.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @return Experience[]
     */
    public function all(): array
    {
        $q = $this->createQueryBuilder('e')
            ->orderBy('e.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @return Experience[]
     */
    public function list(): array
    {
        $q = $this->createQueryBuilder('e')
            ->where('e.popular = 0')
            ->andWhere('e.promotion = 0')
            ->orderBy('e.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @return Experience[]
     */
    public function popular(): array
    {
        $q = $this->createQueryBuilder('e')
            ->where('e.popular = 1')
            ->orderBy('e.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @return Experience[]
     */
    public function promotion(): array
    {
        $q = $this->createQueryBuilder('e')
            ->where('e.promotion = 1')
            ->orderBy('e.name', 'ASC');
        return $q->getQuery()->getResult();
    }

    /**
     * @param User $user
     * @return Experience[]
     */
    public function favorites(User $user): array
    {
        $q = $this->createQueryBuilder('e')
            ->innerJoin('e.favorites', 'f')
            ->innerJoin('f.user', 'u')
            ->where('u.id = :user')
            ->setParameter('user', $user->getId())
            ->orderBy('e.name');
        return $q->getQuery()->getResult();
    }
}

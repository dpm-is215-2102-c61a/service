<?php

namespace App\Controller;

use App\Classess\ResponseController;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\ExperienceRepository;
use App\Traits\UtilTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    use UtilTrait;

    /**
     * @var ResponseController
     */
    protected $response;

    /**
     * @var CategoryRepository
     */
    private $repository;

    /**
     * @var ExperienceRepository
     */
    private $experienceRepository;

    /**
     * @param CategoryRepository $repository
     * @param ExperienceRepository $experienceRepository
     */
    public function __construct(CategoryRepository $repository, ExperienceRepository $experienceRepository)
    {
        $this->repository = $repository;
        $this->experienceRepository = $experienceRepository;
        $this->response = new ResponseController();
    }

    /**
     * @Route("", name="category_list", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $categories = $this->repository->all();
        $categories = $this->categoriesToArray($categories);

        return $this->json(
            $categories
        );
    }

    /**
     * @Route("/{id}", name="category_show", methods={"GET"})
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response
    {
        $category = $this->repository->byId($id);

        return $this->json(
            is_null($category) ? [] : $category->toArray()
        );
    }

    /**
     * @Route("/{id}/experiences", name="category_experiences", methods={"GET"})
     * @param int $id
     * @return Response
     */
    public function experiences(int $id): Response
    {
        $category = $this->repository->byId($id);
        if (is_null($category)) {
            return $this->json([]);
        }

        $experiences = $this->experienceRepository->byCategory($category);
        $experiences = $this->experiencesToArray($experiences);

        return $this->json(
            $experiences
        );
    }

    /**
     * @Route("", name="category_save", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function save(Request $request): Response
    {
        $data = json_decode($request->getContent(), true);

        $name = $data['name'];
        $image = $data['image'];

        $category = new Category();
        $category->setName($name);
        $category->setImage($image);
        $category = $this->repository->save($category);

        return $this->json(
            is_null($category) ? [] : $category->toArray()
        );
    }

    /**
     * @Route("/{id}", name="category_update", methods={"PUT"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, int $id): Response
    {
        $data = json_decode($request->getContent(), true);

        $category = $this->repository->byId($id);
        if (is_null($category)) {
            return $this->json(
                []
            );
        }

        if (!empty($data['name'])) {
            $category->setName($data['name']);
        }

        if (!empty($data['image'])) {
            $category->setImage($data['image']);
        }

        $category = $this->repository->update($category);

        return $this->json(
            is_null($category) ? [] : $category->toArray()
        );
    }

    /**
     * @Route("/{id}", name="category_delete", methods={"DELETE"})
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response
    {
        $category = $this->repository->byId($id);
        if (is_null($category)) {
            return $this->json(
                false
            );
        }

        $this->repository->delete($category);

        return $this->json(
            true
        );
    }
}

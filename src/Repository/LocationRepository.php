<?php

namespace App\Repository;

use App\Entity\Experience;
use App\Entity\Location;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Location|null find($id, $lockMode = null, $lockVersion = null)
 * @method Location|null findOneBy(array $criteria, array $orderBy = null)
 * @method Location[]    findAll()
 * @method Location[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LocationRepository extends ServiceEntityRepository
{
    protected $em;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $manager)
    {
        parent::__construct($registry, Location::class);
        $this->em = $manager;
    }

    /**
     * @param Location $location
     * @return Location
     */
    public function save(Location $location): Location
    {
        $this->em->persist($location);
        $this->em->flush();
        return $location;
    }

    /**
     * @param Location $location
     * @return Location
     */
    public function update(Location $location): Location
    {
        $this->em->persist($location);
        $this->em->flush();
        return $location;
    }

    /**
     * @param Location $location
     */
    public function delete(Location $location)
    {
        $this->em->remove($location);
        $this->em->flush();
    }

    /**
     * @param string $latitude
     * @param string $longitude
     * @param Experience $experience
     * @return Location|null
     */
    public function byLatitudeLongitudeAndExperience(string $latitude, string $longitude, Experience $experience): ?Location
    {
        $q = $this->createQueryBuilder('l')
            ->innerJoin('l.experience', 'e')
            ->where('l.latitude = :latitude')
            ->andWhere('l.longitude = :longitude')
            ->andWhere('e.id = :experience')
            ->setParameters(['latitude' => $latitude, 'longitude' => $longitude, 'experience' => $experience->getId()]);
        try {
            return $q->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
